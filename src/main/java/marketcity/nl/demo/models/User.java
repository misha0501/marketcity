package marketcity.nl.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;

@JsonIgnoreProperties(value = { "password" })  // password wont be shown in JSON object
public class User {
    private int id;
    private static int nextID = 1;
    private String login, password, avatar_url, name, phoneNumber;
    private HashMap<Integer, Item> userItems;

    public User(String login, String password, String avatar_url, String name, String phoneNumber) {
        this.login = login;
        this.password = password;
        this.avatar_url = avatar_url;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.userItems = new HashMap<>();
        this.id = nextID;
        nextID++;
    }

    public Item addItem(Item item) {
        return userItems.put(item.getId(), item);

    }
    public int getId() {
        return id;
    }

    public HashMap getItems() {
        return userItems;
    }

    public ArrayList getItemsArrayList() {
        return new ArrayList(userItems.values());
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\''  + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
