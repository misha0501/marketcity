package marketcity.nl.demo.models;

import java.util.ArrayList;

public class Item {
    private int id, price;
    private static int nextID = 1;
    private String title, image_url, description;
    private ArrayList<Comment> comments;
    private int userId;

    public Item(int price, String title, String image_url, String description, int userId) {
        this.price = price;
        this.title = title;
        this.image_url = image_url;
        this.description = description;
        this.comments = new ArrayList<>();
        this.userId = userId;
        this.id = nextID;
        nextID++;
    }
    public Item() {
        comments = new ArrayList<>();
        this.id = nextID;
        nextID++;
    }


    public void addComment(Comment comment) {

        comments.add(0, comment);
    }

    public void deleteCommentById(int id) {
        Comment comment = findCommentById(id);
        comments.remove(comment);
    }

    public ArrayList<Comment> getCommentsArrayList() {
        return comments;
    }

    public Comment findCommentById(int id) {
        for (Comment comment : comments) {
            if (comment.getId() == id) return comment;
        }
        return null;
    }

    public int getUserID() {
        return userId;
    }

    public void setUserID(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", price=" + price +
                ", title='" + title + '\'' +
                ", image_url='" + image_url + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
