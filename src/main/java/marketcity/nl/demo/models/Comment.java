package marketcity.nl.demo.models;


import marketcity.nl.demo.database.DataProvider;

import java.util.Date;

public class Comment {
    private int id, userId, itemId;
    private static int nextId = 1;
    private String text, userName;
    private Date date;



    public Comment(String text, Date date, int userId, int itemId) {
        this.text = text;
        this.date = date;
        this.itemId = itemId;
        this.userId = userId;
        this.id = nextId;
        nextId++;
    }

    public Comment() {
        this.date = new Date();
        this.userId = -1;
        this.itemId = -1;
        this.id = nextId;
        nextId++;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }


    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return DataProvider.findUserByID(userId).getName();
    }

    public String getUserPicture() {
        return DataProvider.findUserByID(userId).getAvatar_url();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", user=" + userId +
                '}';
    }
}
