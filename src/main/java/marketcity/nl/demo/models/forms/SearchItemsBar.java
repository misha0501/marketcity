package marketcity.nl.demo.models.forms;

public class SearchItemsBar {
    String prefix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
