package marketcity.nl.demo.requestControllers;

import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.forms.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/login")
public class RequestControllerAuth {

    @GetMapping("")
    public String getLogin(HttpSession session, Model model) {
        if (DataProvider.isLoggedIn(session)) {
            return "redirect:/profile";
        }

        // Error message is shown when for example not logged in user try to post a comment or try to sell a product.
        String errormessage = (String) session.getAttribute("errormessage");
        if ( errormessage != null ) {
            model.addAttribute("errormessage", errormessage);
            session.removeAttribute("errormessage");
        }
        return "login";
    }

    @PostMapping("")
    public String authorization(HttpSession session, LoginForm loginForm, Model model) {
        if (DataProvider.loginInputValid(loginForm)) {
            //Create a session and add username
            session.setAttribute("user", DataProvider.findUserByUsername(loginForm.getUsername()));

            //And redirect user to profile page
            return "redirect:/profile";
        } else {
            //Add a message if it's not a valid username or password to show in the login form
            model.addAttribute("errormessage", "Username or Password is not valid!");
            return "login";    //Redirect to the login getter
        }
    }
}
