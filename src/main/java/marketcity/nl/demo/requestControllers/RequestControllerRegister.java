package marketcity.nl.demo.requestControllers;


import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.forms.RegistrationForm;
import marketcity.nl.demo.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/register")
public class RequestControllerRegister {

    @GetMapping("")
    public String register(HttpSession session) {
        if (DataProvider.isLoggedIn(session)) return "redirect:/profile";

        return "register";
    }

    @PostMapping("")
    public String register(HttpSession session, RegistrationForm registrationForm, Model model) {
        if (!DataProvider.userNameIsUnique(registrationForm.getLogin())) {
            model.addAttribute("errormessage", "This login already exists.");
            return "register";
        }
        if (!DataProvider.passwordsMatching(registrationForm.getPassword(), registrationForm.getRepeatPassword())) {
            model.addAttribute("errormessage" , "Passwords don't match");
            return "register";
        }
        User newUser = DataProvider.registerNewUser(registrationForm);

        session.setAttribute("user", newUser);

        return "redirect:/profile";
    }
}
