package marketcity.nl.demo.requestControllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/logout")
public class RequestControllerLogout {


    @GetMapping("")
    public String logout(HttpSession session) {
        session.invalidate();

        return "redirect:/login";
    }
}
