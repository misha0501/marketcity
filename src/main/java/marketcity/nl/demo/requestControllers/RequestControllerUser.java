package marketcity.nl.demo.requestControllers;


import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.forms.UpdateUserForm;
import marketcity.nl.demo.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/profile")
public class RequestControllerUser {

    @GetMapping("")
    public String getUser(HttpSession session, Model model) {
        if (!DataProvider.isLoggedIn(session)) return "redirect:/login";

        User user = (User) session.getAttribute("user");
        model.addAttribute("user", user);

        return "profile";
    }

    @PostMapping("")
    public String updateUser(HttpSession session, Model model, UpdateUserForm updateUserForm) {
        if (!DataProvider.isLoggedIn(session)) return "redirect:/login";
        User user = (User) session.getAttribute("user");

        user = DataProvider.updateUser(user, updateUserForm);

        model.addAttribute("user", user);

        return "profile";
    }

}
