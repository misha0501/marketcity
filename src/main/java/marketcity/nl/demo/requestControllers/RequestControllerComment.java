package marketcity.nl.demo.requestControllers;


import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.Comment;
import marketcity.nl.demo.models.Item;
import marketcity.nl.demo.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/comment")
public class RequestControllerComment {

    @PostMapping("")
    public String postComment(HttpSession session, Comment comment) {
        if (!DataProvider.isLoggedIn(session)){
            session.setAttribute("errormessage", "Login before posting something");
            return "redirect:/login";
        }

        User user = (User) session.getAttribute("user");
        Item item = (Item) session.getAttribute("currentItem");

        comment.setUserId(user.getId());
        comment.setItemId(item.getId());

        DataProvider.addComment(item, comment);

        return "redirect:/item/" + item.getId();
    }

    @GetMapping("/delete/{id}")
    public String deleteComment(HttpSession session, @PathVariable int id) {
        if (!DataProvider.isLoggedIn(session)) return "redirect:/login";
        User user = (User) session.getAttribute("user");
        Item item = (Item) session.getAttribute("currentItem");
        Comment comment = DataProvider.findCommentById(id, item);

        if(comment.getUserId() != user.getId()) return "Not allowed to delete this comment";

        DataProvider.deleteCommentByiD(id, item);

        return "redirect:/item/" + item.getId();

    }
}
