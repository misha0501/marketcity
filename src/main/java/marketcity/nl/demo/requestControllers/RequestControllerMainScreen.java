package marketcity.nl.demo.requestControllers;

import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/")
public class RequestControllerMainScreen {

    @GetMapping("/")
    public String getItems(Model model, @RequestParam(value = "search", required = false) String keyword, HttpSession session, HttpServletRequest request) {
        if (DataProvider.isLoggedIn(session)) model.addAttribute("user", (User) session.getAttribute("user"));

        if (keyword == null || keyword.isEmpty()) {
            model.addAttribute("items", new ArrayList<>(DataProvider.itemsHashMap.values()));
        } else {
            model.addAttribute("items", DataProvider.findItemsByKeyWord(keyword));
        }

        Cookie lastVisitedItem = WebUtils.getCookie(request, "lastVisitedItem");

        if (lastVisitedItem != null) model.addAttribute("lastVisitedItem", lastVisitedItem.getValue());

        return "index";
    }

}
