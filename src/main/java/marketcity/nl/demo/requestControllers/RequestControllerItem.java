package marketcity.nl.demo.requestControllers;


import marketcity.nl.demo.database.DataProvider;
import marketcity.nl.demo.models.Item;
import marketcity.nl.demo.models.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/item")
public class RequestControllerItem {

    @GetMapping("")
    public String getCreateItemPage (HttpSession session, Model model ) {
        if(!DataProvider.isLoggedIn(session)){
            session.setAttribute("errormessage", "Login before selling items");
            return "redirect:/login";
        }

        User user = (User) session.getAttribute("user");

        model.addAttribute("user", user);

        return "create_item";
    }

    @PostMapping("")
    public String createItem (HttpSession session, Item item ,Model model) {
        if(!DataProvider.isLoggedIn(session)){
            session.setAttribute("errormessage", "Login before selling items");
            return "redirect:/login";
        }

        User user = (User) session.getAttribute("user");
        model.addAttribute("user", user);

        item.setUserID(user.getId());
        DataProvider.addItem(item, item.getUserID());

        return "redirect:/item/" + item.getId();
    }

    @GetMapping("/{id}")
    public String getItem (@PathVariable ("id") int id, Model model, HttpSession session, HttpServletResponse response) {
        Item item = DataProvider.findItemByID(id);
        if(item == null) return null;
        if (DataProvider.isLoggedIn(session)) model.addAttribute("user", (User) session.getAttribute("user"));

        Cookie lastVisitedItem = DataProvider.getLastVisitedItemCookie(id);

        model.addAttribute("itemOwner", DataProvider.findUserByID(item.getUserID()));
        model.addAttribute("item", item);

        session.setAttribute("currentItem", item);
        response.addCookie(lastVisitedItem);

        return "item";
    }
}
