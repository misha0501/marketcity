package marketcity.nl.demo.database;

import marketcity.nl.demo.models.*;
import marketcity.nl.demo.models.forms.LoginForm;
import marketcity.nl.demo.models.forms.RegistrationForm;
import marketcity.nl.demo.models.forms.UpdateUserForm;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.util.*;

public class DataProvider {

    public static Map<Integer, Item> itemsHashMap;
    public static Map<Integer, User> usersHashMap;

    static {
        itemsHashMap = new HashMap<>();
        usersHashMap = new HashMap<>();

        String picture = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAM1BMVEUKME7///+El6bw8vQZPVlHZHpmfpHCy9Ojsbzg5ekpSmTR2N44V29XcYayvsd2i5yTpLFbvRYnAAAJcklEQVR4nO2d17arOgxFs+kkofz/154Qmg0uKsuQccddT/vhnOCJLclFMo+//4gedzcApf9B4srrusk+GsqPpj+ypq7zVE9LAdLWWVU+Hx69y2FMwAMGyfusLHwIpooyw9IAQfK+8naDp3OGHvZ0FMhrfPMgVnVjC2kABOQ1MLvi0DEIFj1ILu0LU2WjNRgtSF3pKb4qqtd9IHmjGlJHlc09IHlGcrQcPeUjTAySAGNSkQlRhCCJMGaUC0HSYUx6SmxFAtJDTdylsr4ApC1TY0yquKbCBkk7qnYVzPHFBHkBojhVJWviwgPJrsP4qBgTgbQXdsesjm4pDJDmIuswVZDdFx0ENTtkihoeqSDXD6tVxOFFBHndMKxWvUnzexpIcx/Gg2goJJDhVo6PCMGRAnKTmZuKm3wcJO/upphUqUHy29yVrRhJDORXOKIkEZDf4YiRhEF+iSNCEgb5KY4wSRDkB/yurUEG8nMcocgYABnvbrVL3nMIP0h/d5udKnwzSC/InfPdkJ6eWb0PJE++dyVVyQP5iQmWW27X5QG5druEKafBu0Hqu9saVOHa8HKC/K6BzHKZiRMEZCDF0Nd1/ZfXI/fcOibHOssFgokg9uFA20BhztHEAZIjIohrD/o1wljeFBDEwBo8YUt5Ir/rNLjOIACPFdy/AbEcPdcJBOCxytjeYAM4Kzp6rhOIPhRGNzwmFP3rOoTFI0irtnQKx6fj1Zt+h9njEUS9mKJxfFRrX5lt7wcQtaWTOfTHeIXVJQcQrRW+OYex2j0a66XZINoO8a7fPH2iHF2mC7ZBtB3Czb5QvjizSx7A3308mRzqAwujSywQbYfwc0iU8zqjS0yQ6ztEHX9332KCaGNIYB/Qq1z3yN0oDZBWyeFYJBCkm2sXLhDtpKFwNDMu5TnrZpYGiHbK4Nlwikg5DrYV1g6iPoJmzE5MKd/fOp53EPUaQZaLqH3u+vo2ELWp3wSyWuYGoj9EEIJoV3L9AUS/ZLsJpLNBXmqOu0CW6P5A/dx9IL0FAji/FYKot9EqE0Tvs6QBUe/2CxMEkZAlBNGPhdoAQWyTSmbxUwvUygwQyMmniAPgLt87CODXHuftWJIQgzrfQDC5AfwSgz9MmmG/gWCOqDgZ4JsQeTvZBoJJDhAFEsSDyxUEEUUekk0UEMhjBcEcGsoWVpBU3NcCgkkPkJWrKbdRZvULCMTWhYEdMrayBQRyqHcnSLmAIH7LcWJ8Hch7BsHEdWFpJsZjziCgFBpZ9TPm4e0XBJTTJKt9xjy8RoLI4gimPLP5goCSgWTrEcyzsy8IqmZVMo0H5bJiQToBCOjZ5RcElhjLN3dU7uQMAvoxwQkJZKI1CQzCthJYEigahHuDDi4rFwzCPQ7F1fiDQZgTR5iJwEGYRgIsiECD8BwwMAEfDcIaW8CRBQdhjS1kJQEchDEFhiRKr4KDFPS9FGQNVwEHoW83QjsEHdkfnuIOl6C1NjMItiaCaCWgbdpFJXQ9soh2uoB9aJcCxFdgZwlcrTmvENGlrITBBdpK25Qhd1F2RScq8CKu/gsCL8qN5THjy+Rr5E6joYgPxpdl518QrCf8Kpgjn6C8HLkbb+vt7ZM8wdVvy258khsRfHaS5DalDnlidZT7Erk+SXV5Bj1D3LS29XyhVJuoKHs9Q8S6reK11oUc7vPcr9uswP3SLiDINefXOF5rwCuGzVT6zVkVPfh2wWmHcz4wAwba2cgN1/Tsvleu7//i69CgVyt1GwjOs2+XK3rtbl151Tg3vOeioG40Mz2V+6pQ4xbJHOZj6g0EMxk93tV7fuedvVZpQSPhbwNBGInrymGrwNh1GXmL8F+lAaJ+NU/fzcmvJqvKj7177+1v1GY/GiBKI1Fdy/2XK6upXwaIJpI8B/399W0mH9zzafKaeCF9J0WF+jyCuFusTGzZKhFH8dVLZql2brxgcdVBKb7KG/7UZTmB3XJ6uL/QYT5ScRI74FcHEJ7feopyfGkaeaGlPoCw/BbjZmSBWIvINQNmTxdjWJqwUI8sztR4nYPuIPSTSUnOCZOE3ierqRoJfNSQxDjLEYs8i91eqgFCDSWiFHiuqAN9CwEGCPEISVjvwhS7Mfx6dtX8kC5aqvneGBOEFN2v6RBiYwr3DQOkLhEW6fHFbIwFQnkLiWYmZxE220z/aedPx99C+hiyKR4OzNFhg8S75CJTnxQ1dyugHTLaY10iu9dBpmhQtMz1ABLrkgtHVnRsPUO3OcU25i8cWdGxZbflCBKJqBdMs3aF/dYhNexU9RFcYEmLXYQKghyWdufyldBSU3KpjkKhZclxTXQGCTkL/HZDUIH5+Gkt4SgoCtj7pSYSNJLTK3VVRnmXZxebSMBIzmHABeIdXBebiN9eHYtUZ62ab3BdGkUm+SKJw1bdRXeewaX7qqdAnljg2sVxg3guAk3baofcg9yZ2eZpnHNvSFrEqhB9YPjesmt0pt6Xc8hl7W5L9Q4Xx09ctsrd5VhWeF6nF8SRrZdw49qns//0xTK/AZ8vGr3caTliuzeFNeCJTgafpKlhHd2WP1sy1LqDF798gjKJPLqDr9keoTd43+NyNzC1CI8Xy2lcPtOaVBI5IiAWyQ3e125AcKoXs2Djhy5eVc3KiBxREIPkhjBiLhIjU++4T91IbggjRiCJLSEIwWGddkEaxlVN5KCArPHk8mXVpHk8FHH7JL3n5dPA7C90q7XkeFJucacNmGXeRfswLE71HA79efaGiCN/Ofjmfmtcp8X10tIsqCacV5xfRWjNUiXGYbovWgyFYHcQLak15K9oM5zqmgaeKsHJetbSHfSPzXOiw/rxE9YH4CXaUpsZ0ztemFurP95Jpyvrd29YTpIZr7cEJHqfc7Wl0PFm2+yJR70udaokKFtGPTdm8WdQe24+HmVLlueboWQquBcYYVH2vEzfh8kCks1p90eWsLCyZ8qK7E86Oe+3XYFnBuiWdth20UqZR5SvMoyPg3WNauJipi0LMTQgVq5xUUlZcrPsopPHJ926z8pm7xyFLrH/PxpHSoXKdWgXsLn1scZn1ZDd/2vszN3lt254qkE+qu3yoqLM+ghN3Qz2qcVzUC/ZMFsK/alU6l0OWV/bQz6v6yYbyuN5BaZ4A7Y30vs/PPksS2+qzlvfF7OQmzzcL7W+xa7OIfRuVdtn/tdvdFLnL4OTKcm2W16PmWc4FWWXNSlWM2n3D+uPxuyrcfo74aP+Ac30a82+oLmfAAAAAElFTkSuQmCC";
        User user = new User("RogierCool", "12345", "https://lh3.googleusercontent.com/eihALSTnLousbALvHTGXcLQ4-ngV3zdsgMZp0ytXgD8Hz04A4a9DU-MerEpcABFVgXbGhw=s64", "Rogier Hommels", "0677775555");
        User user2 = new User("Misha", "12345", picture, "Mykhailo", "0633357772");
        User user3 = new User("1", "1", picture, "Misha", "0633333333");
        User user4 = new User("John", "12345", picture, "John", "0633333336");
        User user5 = new User("Sasha", "12345", picture, "Sasha", "0633333337");


        // adding users to the administration
        addUser(user);
        addUser(user2);
        addUser(user3);
        addUser(user4);
        addUser(user5);

        Item item = new Item(35, "Clean Code", "https://media.s-bol.com/3744jK29EQR/550x730.jpg", "A book with lots of useful information for coders", user.getId());
        Item item3 = new Item(155, "Set of cigars", "https://www.cigarsunlimited.co.uk/acatalog/1095-cohiba-piramides-extra-cuban-cigars-box-10.jpg", "A new set of Mexican cigars.", user2.getId());
        Item item4 = new Item(10000, "Toyota Corola", "https://media.caradvice.com.au/image/private/c_fill,q_auto,f_auto,w_auto/wxszil7ld3xmvkor4cvv.jpg", "Almost new car that starts from the first time", user.getId());
        Item item2 = new Item(15, "Spinner", "https://www.pascogifts.com/files/cache/medium/files/fidget-spinner-bedrukken-5911e2092acae.jpg", "Spinner is bought 5 days ago. Without damage.", user2.getId());
        Item item5 = new Item(85, "Book4", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTDNAMjTMfZlpsHECkRfZw30HZLPisdm6zcuEBLW_ZLWsXQ24dr&usqp=CAU", "A book with lots of interestion shit", user2.getId());

        addComment(item, new Comment("Nice book, available?", new Date(), 2, 1));
        addComment(item, new Comment("Never seen this book before, can I have it for 30?", new Date(), 4, 2));
        addComment(item, new Comment("Nice book, every programmer should read it?", new Date(), 3, 3));

        // adding items to admin class
        addItem(item, item.getUserID());
        addItem(item2, item2.getUserID());
        addItem(item3, item3.getUserID());
        addItem(item4, item4.getUserID());
        addItem(item5, item5.getUserID());
    }


    public static void addUser(User user) {
        usersHashMap.put(user.getId(), user);
    }

    public static void addItem(Item item, int userId) {
        itemsHashMap.put(item.getId(), item);
        User user = findUserByID(userId);
        user.addItem(item);
    }

    public static void addComment(Item item, Comment comment) {
        item.addComment(comment);
    }

    public static User findUserByID(int id) {
        return usersHashMap.get(id);
    }

    public static Item findItemByID(int id) {
        return itemsHashMap.get(id);
    }

    public static Comment findCommentById(int id, Item item) {
        return item.findCommentById(id);
    }

    public static User findUserByUsername(String username) {
        for (User user : usersHashMap.values()) {
            if (user.getLogin().equals(username)) return user;
        }
        return null;
    }

    public static ArrayList<Item> findItemsByKeyWord(String keyword) {
        ArrayList<Item> result = new ArrayList<>();
        for (Item item : itemsHashMap.values()) {
            if (item.getTitle().toLowerCase().contains(keyword.toLowerCase())) result.add(item);
        }
        return result;
    }

    public static User updateUser(User user, UpdateUserForm updateUserForm) {
        user.setName(updateUserForm.getName());
        user.setPhoneNumber(updateUserForm.getPhoneNumber());
        return user;
    }

    public static void deleteCommentByiD(int id, Item item) {
        item.deleteCommentById(id);
    }


    public static HashMap<String, String > getLoginCredentials() {
        HashMap<String, String> loginCredentials = new HashMap<>();
        for (User user : usersHashMap.values()) {
            loginCredentials.put(user.getLogin(), user.getPassword());
        }
        return loginCredentials;
    }

    public static Cookie getLastVisitedItemCookie(int itemId) {
        Cookie getLastVisitedItem = new Cookie("lastVisitedItem", Integer.toString(itemId));
        getLastVisitedItem.setMaxAge(7 * 24 * 60 * 60); // expires in 7 days
        getLastVisitedItem.setPath("/");

        return getLastVisitedItem;
    }

    public static boolean loginInputValid(LoginForm loginForm) {
        HashMap<String, String> loginCredentials = getLoginCredentials();
        String usernameInput = loginForm.getUsername();
        String passwordInput = loginForm.getPassword();

        if(loginCredentials.get(usernameInput) == null) return false;

        return loginCredentials.get(usernameInput).equals(passwordInput);
    }

    public static boolean isLoggedIn(HttpSession session) {
        return session.getAttribute("user") != null;
    }

    public static boolean userNameIsUnique(String login) {
        HashMap<String, String> loginCredentials = getLoginCredentials();
        for (String key : loginCredentials.keySet()) {
            if(key.equals(login)) return false;
        }
        return true;
    }

    public static boolean passwordsMatching(String password, String repeatPassword) {
        return password.equals(repeatPassword);
    }

    public static User registerNewUser(RegistrationForm registrationForm) {
        String login = registrationForm.getLogin();
        String name = registrationForm.getName();
        String password = registrationForm.getPassword();
        String photo_url = registrationForm.getAvatar_url();
        String phoneNumber = registrationForm.getPhoneNumber();

        User user = new User(login, password, photo_url, name, phoneNumber);
        addUser(user);

        return user;
    }
}
