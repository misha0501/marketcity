package marketcity.nl.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketCityApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketCityApplication.class, args);
    }

}
